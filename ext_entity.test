<?php

/**
 * @file
 * Test classes for ext_entity.
 */

/**
 * External entity test case class.
 */
class ExtEntityWebTestCase extends DrupalWebTestCase {

  /**
   * The test entity type.
   *
   * @var string
   */
  protected $entityType = 'ext_test';

  /**
   * The number of test entities to create data for.
   *
   * @var integer
   */
  protected $entityCount = 5;

  /**
   * @var array
   */
  protected $entityData = array();

  public static function getInfo() {
    return array(
      'name' => 'External entity web tests',
      'description' => 'Web tests for external entity functionality',
      'group' => 'External entity',
    );
  }

  public function setUp() {
    parent::setUp('entity', 'ext_entity', 'ext_test_entity', 'entitycache');

    $this->extTestEntityData();
  }

  /**
   * Tests entity CRUD methods.
   */
  public function testEntity() {
    // Test basic entity info is present.
    $info = entity_get_info($this->entityType);
    $this->assertTrue(!empty($info), 'Entity info found for ext_test entity.');
    $this->assertEqual($info['database connection'], 'default');
    $this->assertEqual($info['database base table'], 'ext_test_entity');

    $controller = entity_get_controller($this->entityType);

    // Test the controller::load() method.
    $all = $controller->load(FALSE);
    $this->assertEqual(count($all), $this->entityCount);

    // Load a single entity by ID.
    $entity = reset($controller->load(array(1)));
    $this->assertTrue($entity instanceof $info['entity class']);

    // Load mulitple entities by ID.
    $entities = $controller->load(array(1, 2, 3));
    foreach ($entities as $entity) {
      $this->assertTrue($entity instanceof $info['entity class']);
    }

    // Load entities by bundle.
    $entities = $controller->load(FALSE, array('type' => 'test_1'));
    $this->assertEqual(count($entities), 2);
    foreach ($entities as $entity) {
      $this->assertTrue($entity instanceof $info['entity class']);
    }

    // Load entities by another condition.
    $entities = $controller->load(FALSE, array('status' => FALSE));
    $this->assertEqual(count($entities), 3);
    foreach ($entities as $entity) {
      $this->assertTrue($entity instanceof $info['entity class']);
    }

    // Test updating an entity.
    $entity = reset($controller->load(array(1)));
    $new_title = $this->randomString();
    // Change the title and save.
    $entity->title = $new_title;
    $entity->save();
    // Load it and make sure it has loaded the new title.
    $controller->resetCache(array(1));
    $entity = reset($controller->load(array(1)));
    $this->assertIdentical($entity->title, $new_title, 'A new title was successfully updated on an entity.');

    // Test deleting an entity.
    $entity = reset($controller->load(array(1)));
    $entity->delete();
    $this->assertFalse($controller->load(array(1)), 'An entity was deleted.');

    // Test normalization and preparing values.
    $values = array(
      'alert_id' => 3352517,
      'site_id' => 70547,
      'advisory' => 'node-351',
      'ignored' => FALSE,
      'initial_log_id' => 32708177,
      'latest_log_id' => NULL,
      'failed_value' => 1,
      'cleared' => FALSE,
    );
    $new = $controller->create($values);

    $this->assertIdentical($new->normalize(), $values, 'The normalized data matches the entity create values.');
  }

  /**
   * Tests entity caching related functions/methods.
   */
  public function testEntityCache() {
    $controller = entity_get_controller($this->entityType);
    // Get the cache bin for this entitycache cache table.
    $bin = _cache_get_object('cache_entity_' . $this->entityType);

    // Load all entities so they will be cached.
    $all = $controller->load(FALSE);

    // Get the id of the first entity we have.
    $entity = reset($all);
    $id = $entity->internalIdentifier();

    // Test all entities.
    $cached_ids = array_keys($all);
    $cached = $bin->getMultiple($cached_ids);
    // Get the IDs again, as getMultiple takes IDs by reference.
    $ids = array_keys($all);
    $this->assertEqual(count($ids), count($cached), 'All entities are cached.');
    // Remove all of them (truncate table).
    $controller->invalidateEntityCacheIDs(NULL);
    // Try and load all the cache entries again.
    $cached_ids = array_keys($all);
    // None of the entity IDs should return an cache entry.
    $bin->getMultiple($cached_ids);
    $this->assertTrue(empty($cached_ids), 'All cache entries have been removed.');

    // Test cache stuff with the first entity. Load it, so it will be cached again.
    $controller->resetCache();
    $controller->load(array($id));
    $cache = $bin->get($id);

    $this->assertTrue(!empty($cache->data), 'The entity data was cached.');

    // Invalidate this ID and make sure it isn't loaded from the cache anymore.
    $controller->invalidateEntityCacheIDs($id);
    $this->assertFalse($bin->get($id), 'The entity cache was invalidated.');

    // Try again but pass in an array this time to invalidateEntityCacheIDs().
    $controller->resetCache();
    $controller->load(array($id));
    $cache = $bin->get($id);

    $this->assertTrue(!empty($cache->data), 'The entity data was cached.');

    // Invalidate this ID and make sure it isn't loaded from the cache anymore.
    $controller->invalidateEntityCacheIDs(array($id));
    $this->assertFalse($bin->get($id), 'The entity cache was invalidated.');
  }

  /**
   * Tests EFQ for external entities.
   */
  public function testEFQ() {
    $info = entity_get_info($this->entityType);
    $controller = entity_get_controller($this->entityType);

    // A simple query, just by entity type.
    $query = $this->getBaseEFQ();
    $result = $query->execute();

    $this->assertTrue(isset($result[$this->entityType]));

    // Test that the count and keys match the result from controller::load().
    $all_entities = $controller->load(FALSE);
    $this->assertEqual(count($result[$this->entityType]), count($all_entities));
    $this->assertIdentical(array_keys($result[$this->entityType]), array_keys($all_entities), 'The expected entity IDs were found.');
    // Test each result object contains the correct entity ID.
    $idKey = $info['entity keys']['id'];
    foreach ($result[$this->entityType] as $key => $stub) {
      $this->assertEqual($key, $stub->{$idKey});
    }

    // Test entityCondition bundle.
    $entities = $controller->load(FALSE, array('type' => 'test_1'));
    $query = $this->getBaseEFQ();
    $query->entityCondition('bundle', 'test_1');
    $result = $query->execute();
    $this->assertEqual(count($result[$this->entityType]), count($entities), 'The entity condition returned the correct number of results.');

    // Test a propertyCondition
    $entities = $controller->load(FALSE, array('status' => FALSE));
    $query = $this->getBaseEFQ();
    $query->propertyCondition('status', FALSE);
    $result = $query->execute();
    $this->assertEqual(count($result[$this->entityType]), count($entities), 'The property condition returned the correct number of results.');

    // Test the count query.
    $query = $this->getBaseEFQ();
    $query->count();
    $this->assertEqual($query->execute(), count($all_entities), 'The expected result from a count query was returned.');
  }

  /**
   * Returns a base EFQ object, this the entity_type condition set.
   * @return [type] [description]
   */
  protected function getBaseEFQ() {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);
    return $query;
  }

  /**
   * Adds some test data to the ext_test_entity table.
   */
  protected function extTestEntityData($number_of_entities = 5) {
    // Create a map to alternate values between odd and even rows.
    $map = array(
      'even' => array(
        'type' => 'test_1',
        'status' => TRUE,
      ),
      'odd' => array(
        'type' => 'test_2',
        'status' => FALSE,
      ),
    );

    for ($i = 0; $i < $number_of_entities; $i++) {
      $number_type = ($i % 2) ? 'even' : 'odd';
      $this->entityData[] = array(
        'type' => $map[$number_type]['type'],
        'title' => $this->randomString(),
        'status' => $map[$number_type]['status'],
        'created' => time() - ($i * 1000),
      );
    }

    foreach ($this->entityData as $item) {
      drupal_write_record('ext_test_entity', $item);
    }
  }

}
