<?php

/**
 * @file
 * ext_entity entity classes.
 */

/**
 * ExtEntityControllerBase class.
 */
class ExtEntityControllerBase extends EntityAPIController {

  /**
   * Stores the database connection instance.
   *
   * @var Database
   */
  protected $connection;

  /**
   * Returns entity info for this entity type.
   *
   * @return array
   *   An array of entity info
   *   @see entity_get_info()
   */
  public function getEntityInfo() {
    return $this->entityInfo;
  }

  /**
   * Returns the database connection.
   *
   * @return Database instance
   */
  public function getConnection() {
    if (!isset($this->connection)) {
      $this->connection = Database::getConnection($this->entityInfo['database connection key'], $this->entityInfo['database connection']);
    }

    return $this->connection;
  }

  /**
   * Overrides EntityAPIController::buildQuery().
   */
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    // @todo Add properties for base table etc... to the controller?
    $query = $this->getConnection()->select($this->entityInfo['database base table'], 'base');

    // By default, all table columns will be added to the query.
    // @todo Move this into acquia_nspi_entity of move other property info logic
    // to this module.
    $fields = !empty($this->entityInfo['property info']['base']) ? array_keys($this->entityInfo['property info']['base']) : array();
    $query->fields('base', $fields);

    $query->addTag($this->entityType . '_load_multiple');

    // Add an ID condition if IDs have been specified.
    if (!empty($ids)) {
      $query->condition('base.' . $this->idKey, $ids);
    }
    // Add any conditions as needed.
    if (!empty($conditions)) {
      foreach ($conditions as $field => $value) {
        $query->condition('base.' . $field, $value);
      }
    }

    return $query;
  }

  /**
   * Overrides EntityAPIController::load().
   */
  public function query($ids, $conditions, $revision_id = FALSE) {
    $query = $this->buildQuery($ids, $conditions, NULL);
    $result = $query->execute();

    // Create and return an array of entity objects. Not using PDO::setFetchMode
    // as this does not pass $values into the class constructor.
    if (!empty($this->entityInfo['entity class']) && ($class = $this->entityInfo['entity class'])) {
      $entities = array();
      foreach ($result->fetchAllAssoc($this->idKey, PDO::FETCH_ASSOC) as $key => $values) {
        $entities[$key] = $this->create($values);
      }

      return $entities;
    }

    return $result;
  }

  /**
   * Prepare the loaded entity values before creating an entity instance.
   *
   * @param array $values
   *   The loaded array of entity values, passed by reference.
   */
  protected function prepareLoadValues(array $values) {
    return $values;
  }

  /**
   * Overrides EntityAPIController::load().
   *
   * @see EntityAPIController::load() for doc comments.
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = array();

    $passed_ids = !empty($ids) ? array_flip($ids) : FALSE;

    if ($this->cache) {
      $entities = $this->cacheGet($ids, $conditions);
      if ($passed_ids) {
        $ids = array_keys(array_diff_key($passed_ids, $entities));
      }
    }

    if (!empty($this->entityInfo['entity cache']) && $ids && !$conditions) {
      $cached_entities = EntityCacheControllerHelper::entityCacheGet($this, $ids, $conditions);
      $ids = array_diff($ids, array_keys($cached_entities));
      $entities += $cached_entities;

      if ($this->cache && !empty($cached_entities)) {
        $this->cacheSet($cached_entities);
      }
    }

    if (!($this->cacheComplete && $ids === FALSE && !$conditions) && ($ids === FALSE || $ids)) {
      $queried_entities = array();
      foreach ($this->query($ids, $conditions) as $record) {
        if (isset($entities[$record->{$this->idKey}])) {
          continue;
        }

        $queried_entities[$record->{$this->idKey}] = $record;
      }
    }

    if (!empty($queried_entities)) {
      $this->attachLoad($queried_entities);
      $entities += $queried_entities;
    }

    if (!empty($this->entityInfo['entity cache']) && !empty($queried_entities)) {
      EntityCacheControllerHelper::entityCacheSet($this, $queried_entities);
    }

    if ($this->cache) {
      if (!empty($queried_entities)) {
        $this->cacheSet($queried_entities);

        if (!$conditions && $ids === FALSE) {
          $this->cacheComplete = TRUE;
        }
      }
    }

    if ($passed_ids && $passed_ids = array_intersect_key($passed_ids, $entities)) {
      foreach ($passed_ids as $id => $value) {
        $passed_ids[$id] = $entities[$id];
      }
      $entities = $passed_ids;
    }

    return $entities;
  }

  /**
   * Overrides EntityAPIController::save().
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $transaction = isset($transaction) ? $transaction : db_transaction();

    try {
      if (!empty($entity->{$this->idKey}) && !isset($entity->original)) {
        $entity->original = entity_load_unchanged($this->entityType, $entity->{$this->idKey});
      }

      $this->invoke('presave', $entity);

      if (!empty($entity->{$this->idKey}) && empty($entity->is_new)) {
        $return = $this->getConnection()->update($this->entityInfo['database base table'])
          ->fields($this->prepareSaveValues($entity))
          ->condition($this->idKey, $entity->{$this->idKey})
          ->execute();

        $this->resetCache(array($entity->{$this->idKey}));
        $this->invoke('update', $entity);
      }
      else {
        $return = $this->getConnection()->insert($this->entityInfo['database base table'])
          ->fields($this->prepareSaveValues($entity))
          ->execute();
        $this->invoke('insert', $entity);
      }

      db_ignore_slave();
      unset($entity->is_new);
      unset($entity->original);

      return $return;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception($this->entityType, $e);
      throw $e;
    }
  }

  /**
   * Prepare entity values to be saved.
   *
   * @param object $entity
   *   The entity to extract values from.
   *
   * @return array
   *   A keyed array of values to be saved. These should be keyed by field/column.
   */
  protected function prepareSaveValues($entity) {
    // If there are base table fields, extract those from the entity to save.
    if (!empty($this->entityInfo['property info']['base'])) {
      // If it's empty, assume we want all fields. Otherwise, only get declared base.
      // cast to an array then intersect against the base array.
      return array_intersect_key((array) $entity, $this->entityInfo['property info']['base']);
    }
    // Otherwise just used an normalized array.
    else {
      return $entity->normalize();
    }
  }

  /**
   * Overrides EntityAPIController::delete().
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $entities = $ids ? $this->load($ids) : FALSE;
    if (!$entities) {
      return;
    }

    $transaction = isset($transaction) ? $transaction : db_transaction();

    try {
      $ids = array_keys($entities);

      $this->getConnection()->delete($this->entityInfo['database base table'])
        ->condition($this->idKey, $ids, 'IN')
        ->execute();
      $this->resetCache($ids);

      foreach ($entities as $id => $entity) {
        $this->invoke('delete', $entity);
      }

      db_ignore_slave();
    }
    catch (Exception $e) {
      if (isset($transaction)) {
        $transaction->rollback();
      }
      watchdog_exception($this->entityType, $e);
      throw $e;
    }
  }

  /**
   * Overrides EntityAPIController::invoke().
   */
  public function invoke($hook, $entity) {
    // Invokations of field stuff has been removed here.
    // Invoke the hook.
    module_invoke_all($this->entityType . '_' . $hook, $entity);
    // Invoke the respective entity level hook.
    if ($hook == 'presave' || $hook == 'insert' || $hook == 'update' || $hook == 'delete') {
      module_invoke_all('external_entity_' . $hook, $entity, $this->entityType);
    }
    // Invoke rules.
    if (module_exists('rules')) {
      rules_invoke_event($this->entityType . '_' . $hook, $entity);
    }
  }

  /**
   * Invalidates entitycache cache entries for this entity type.
   *
   * @param mixed $ids
   *   Pass an array of IDs or a single ID to invalidate selected entity caches
   *   by ID, or pass NULL to remove all cache entries for this type (default).
   *
   * @return bool
   *   TRUE if successful, FALSE otherwise.
   */
  public function invalidateEntityCacheIDs($ids = NULL) {
    if ($bin = _cache_get_object('cache_entity_' . $this->entityType)) {
      // If it's not empty, assume it's an array.
      if (!empty($ids)) {
        // Cast to an array incase we have a string/int.
        $ids = (array) $ids;
        foreach ($ids as $id) {
          // Clear the cache entry for this ID.
          // @todo the cache class doesn't have a clear for multiple ids at once?
          // wtf... shall we just implement our own logic to handle this?
          $bin->clear($id);
        }
      }
      // If $ids is NULL, truncate the table.
      elseif (!isset($ids)) {
        // Truncate the table.
        $bin->clear(NULL, '*');
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Overrides EntityAPIController::export().
   *
   * We don't need to implement the $prefix parameter, as we won't be bothered
   * with pretty printing the output?
   */
  public function export($entity, $prefix = '') {
    return json_encode($entity->normalize());
  }

  /**
   * Export multiple entities. Serialize them all together.
   */
  public function exportMulitple(array $entities) {
    $vars = array();

    foreach ($entities as $entity) {
      $vars[$entity->{$this->idKey}] = $entity->normalize();
    }

    return json_encode($vars);
  }

}

/**
 * ExtEntityBase class.
 */
class ExtEntityBase extends Entity {

  /**
   * Normalizes entity properties as an array.
   *
   * @param array $additional_data
   *   Additional data to merge into the normalized entity property data.
   *
   * @return array
   */
  public function normalize(array $additional_data = array()) {
    $class_info = new ReflectionObject($this);

    foreach ($class_info->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
      $name = $property->getName();
      $properties[$name] = $this->{$name};
    }

    unset($properties['is_new'], $properties['original']);

    return $properties += $additional_data;
  }

}

/**
 * ExtEntityMetadataController metadata controller class.
 */
class ExtEntityMetadataController extends EntityDefaultMetadataController {

  /**
   * Overrides EntityDefaultMetadataController::convertSchema().
   *
   * @todo Not sure what to do here yet. Shall we declare properties in
   * hook_entity_property_info()?
   */
  protected function convertSchema() {
    return array();
  }

}

/**
 * Default controller for generating basic views integration.
 *
 * The controller tries to generate suiting views integration for the entity
 * based upon the schema information of its base table and the provided entity
 * property information.
 * For that it is possible to map a property name to its schema/views field
 * name by adding a 'schema field' key with the name of the field as value to
 * the property info.
 */
class ExtEntityViewsController extends EntityDefaultViewsController {

  /**
   * Defines the result for hook_views_data().
   */
  public function views_data() {
    $data = array();
    $this->relationships = array();

    if (!empty($this->info['database base table'])) {
      $table = $this->info['database base table'];
      // Define the base group of this table. Fields that don't
      // have a group defined will go into this field by default.
      $data[$table]['table']['group']  = drupal_ucfirst($this->info['label']);
      $data[$table]['table']['entity type'] = $this->type;

      $data[$table]['table']['base'] = array(
        'field' => $this->info['entity keys']['id'],
        'title' => drupal_ucfirst($this->info['label']),
        'help' => isset($this->info['description']) ? $this->info['description'] : '',
        'database' => $this->info['database connection'],
      );
      $data[$table]['table']['entity type'] = $this->type;
      //$data[$table] += $this->schema_fields();

      // Add in any reverse-relationships which have been determined.
      $data += $this->relationships;
    }

    return $data;
  }

}
