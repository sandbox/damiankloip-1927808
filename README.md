# External entities

## Overview

This module provides a generic entity and controller class for loading external
database tables as entities.

One of the main tasks of this is to remove/bypass Drupal's assumption that entities
will be loaded from Drupal's database, and will use schema information declared
in hook_schema(). For this reason, implementations don't use the 'base table'
key in hook_entity_info() to circumvent presumptions to load schmea info.

A custom executeCallback is implemented for EntityFieldQuery, so this can be used
as normal to query these external entities using entity/property conditions as
normal.

The entity code for loading revisions is also removed, to keep these entities
lean and mean. As well as field api integration, for a similar reason. So all
entity data will be loaded as a property of the entity.