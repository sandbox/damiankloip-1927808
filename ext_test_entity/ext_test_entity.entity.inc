<?php

/**
 * @file
 * ext_entity_test entity classes.
 */

/**
 * ExtTestEntityController class.
 */
class ExtTestEntityController extends ExtEntityControllerBase {

  /**
   * Overrides ExtEntityControllerBase::attachLoad().
   */
  protected function attachLoad(&$queried_entities, $revision_id = FALSE) {
    // Any computed values etc... could be added here.
    // foreach ($queried_entities as $entity) {
    //   $entity->computedValue = 'SOME COMPUTED VALUE';
    //   $entity->someOtherComputedValue = array(
    //     'one' => 'One',
    //     'two' => 'Two',
    //   );
    // }
  }

}

/**
 * ExtTestEntity class.
 */
class ExtTestEntity extends ExtEntityBase {

  //public $computedValue;
  //public $someOtherComputedValue = array();

}
